Note (April 2019):
Maintained on github.com and updated by:
Aravindh Krishnamoorthy
aravindh.krishnamoorthy@fau.de

WinTeddy 1.0 (10/97)
Written by Claudio Felber, Cybernetic (felber@cybernetic.ch)

DESCRIPTION

WinTeddy is a port of the great X Windows application xteddy.
It doesn't really do anything useful except hanging around on
your desktop and wasting precious space by always staying on
top of every other window you have opened (just like it's
good friend on the X Windows system).

WinTeddy can be moved around with the mouse. A click with the
right mouse button on the teddy gives you a small popup menu
with a few commands. "About" once more tells you where people
live that can spend all their time writing useless software
(...well, err... not really!), "Default position" moves the
teddy back to its favourite position at the top right corner
of the screen. This is also useful if you have resized your
desktop to a smaller resolution and have thus lost your
teddy in nowhereland. "Exit" just does what you expect it
to do.

Update (04.2019):
WinTeddy now uses a cursor similar to XC_heart from XWindows.

COMPILATION

WinTeddy is ready for execution in the file WinTeddy.exe.
If you nevertheless want to compile it yourself (maybe you
have made some changes) you can use the makefile WinTeddy.mak
like that:

	nmake /f "WinTeddy.mak" CFG="WinTeddy - Win32 Release"

The makefiles where made for Visual C++ 5.0. But the source
code is pure standard Win32 SDK code. It should compile with
any 32 bit Windows C-Compiler.

Note: This command is also present in the batch file "make.bat" for convenience.

CUSTOMIZATION

If you don't like the teddy bear image you can easily (well...
depends who you are...) provide your own. Name your file
Teddy.bmp and calculate region mask data with the small utility
program WinTeddyInit:

	WinTeddyInit Teddy.bmp

This will result in the file Teddy.rgn which will automatically
be linked with your program.


KNOWN BUGS

Pfff... don't know...


AUTHOR

Maintained and updated by Aravindh Krishnamoorthy, 2019:
Email address: aravindh.krishnamoorthy@fau.de

Written by Claudio Felber, Cybernetic, 1997.
Email address: felber@cybernetic.ch
Homepage: www.cybernetic.ch

WinTeddy is based on xteddy for X Windows.
Original idea and program by Stefan Gustavson, 
ISY-LiTH (stegu@itn.liu.se)

The teddy bear images are photos of a Tender Teddy from Gund.
WinTeddy is funware and may be freely copied and distributed.


DISCLAIMER

I make no promises that this software will work for any purpose
whatsoever. If you hurt yourself or your system, don't blame me.
Blame WinTeddy if you like. He doesn't mind.


FILES

WinTeddy.exe		This is the actual program file of 
			WinTeddy
Readme.txt		This file
WinTeddy.cpp	The source code of the WinTeddy program
WinTeddyInit.cpp	A program used to calculate region mask 
			data for the teddy shape
WinTeddy.rc	Dialog, bitmap, icon and region data 
			resources
WinTeddy.ico	Program icon
WinTeddy.mak	Makefile for WinTeddy
WinTeddyInit.mak	Makefile for WinTeddyInit utility
Teddy.bmp	Teddy bear image
Teddy.rgn	Region data calculated from teddy bear
			image
Title.bmp	WinTeddy logo in "About" dialog box
Resource.h	IDs of the various resources used throughout 
			the program
XC_heart.cur Windows cursor similar to the XWindows XC_heart cursor.
make.bat     Compiles the program in "Developer Command Prompt for Visual Studio"
