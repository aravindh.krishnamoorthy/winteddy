// This program is used to calculate the region data describing
// the teddy bear's shape. This process is far too time consuming
// to have it done everytime the teddy is started. That's why
// it is precalculated here. The region data is then stored
// in a file with the extension .rgn and linked to the resource
// data of the teddy application.

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

void main(int argc, char** argv)
{
    if (argc != 2) {
        printf("Usage: WinTeddyInit <BitmapFile>\n");
        exit(EXIT_FAILURE);
    }

    HBITMAP hbm = (HBITMAP)LoadImage(NULL, argv[1], IMAGE_BITMAP, 0, 0,
        LR_CREATEDIBSECTION | LR_DEFAULTSIZE | LR_LOADFROMFILE);

    if (hbm == NULL) {
        printf("Could not load image.\n");
        exit(EXIT_FAILURE);
    }

    printf("Working...\n");

    HDC hdc = CreateCompatibleDC(NULL);
    HBITMAP hbmOld = (HBITMAP)SelectObject(hdc, hbm);

    DIBSECTION dbs;
    GetObject(hbm, sizeof(DIBSECTION), &dbs);

    LONG nWidth = dbs.dsBm.bmWidth;
    LONG nHeight = dbs.dsBm.bmHeight;

    COLORREF crTrans = GetPixel(hdc, 0, 0);
    DWORD nPixelCount = 0;

    for (int y = 0; y < nHeight; y++)
        for (int x = 0; x < nWidth; x++)
            if (GetPixel(hdc, x, y) != crTrans)
                nPixelCount++;

    HRGN hrgn = CreateRectRgn(0, 0, 0, 0);
    for (int y = 0; y < nHeight; y++)
        for (int x = 0; x < nWidth; x++)
            if (GetPixel(hdc, x, y) != crTrans) {
                HRGN hrgnTemp = CreateRectRgn(x, y, x + 1, y + 1);
                CombineRgn(hrgn, hrgn, hrgnTemp, RGN_OR);
                DeleteObject(hrgnTemp);
            }

    DWORD dwSize = GetRegionData(hrgn, 0, 0);
    PRGNDATA prgndata = (PRGNDATA)HeapAlloc(GetProcessHeap(), 0, dwSize);
    GetRegionData(hrgn, dwSize, prgndata);

    char szPath[256];
    lstrcpy(szPath, argv[1]);
    
    char* pPath = szPath + lstrlen(szPath);
    while (--pPath >= szPath)
        if (*pPath == '.') {
            *pPath = 0;
            break;
        }

    lstrcat(szPath, ".rgn");
    puts(argv[1]);
    puts(szPath);

    FILE* file = fopen(szPath, "wb");
    if (!file) {
        printf("Could not create region file.\n");
        exit(EXIT_FAILURE);
    }

    fwrite(prgndata, 1, dwSize, file);
    fclose(file);

    DeleteObject(hrgn);
    SelectObject(hdc, hbmOld);
    DeleteDC(hdc);

    printf("Done.\n");
}
