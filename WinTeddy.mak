# Microsoft Developer Studio Generated NMAKE File, Based on WinTeddy.dsp
!IF "$(CFG)" == ""
CFG=WinTeddy - Win32 Debug
!MESSAGE No configuration specified. Defaulting to WinTeddy - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "WinTeddy - Win32 Release" && "$(CFG)" !=\
 "WinTeddy - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "WinTeddy.mak" CFG="WinTeddy - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "WinTeddy - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "WinTeddy - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "WinTeddy - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\WinTeddy.exe"

!ELSE 

ALL : "WinTeddyInit - Win32 Release" "$(OUTDIR)\WinTeddy.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"WinTeddyInit - Win32 ReleaseCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\WinTeddy.obj"
	-@erase "$(INTDIR)\WinTeddy.res"
	-@erase "$(OUTDIR)\WinTeddy.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS"\
 /Fp"$(INTDIR)\WinTeddy.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC_PROJ=/l 0x807 /fo"$(INTDIR)\WinTeddy.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\WinTeddy.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib /nologo /subsystem:windows /incremental:no\
 /pdb:"$(OUTDIR)\WinTeddy.pdb" /machine:I386 /out:"$(OUTDIR)\WinTeddy.exe" 
LINK32_OBJS= \
	"$(INTDIR)\WinTeddy.obj" \
	"$(INTDIR)\WinTeddy.res"

"$(OUTDIR)\WinTeddy.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "WinTeddy - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\WinTeddy.exe"

!ELSE 

ALL : "WinTeddyInit - Win32 Debug" "$(OUTDIR)\WinTeddy.exe"

!ENDIF 

!IF "$(RECURSE)" == "1" 
CLEAN :"WinTeddyInit - Win32 DebugCLEAN" 
!ELSE 
CLEAN :
!ENDIF 
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\WinTeddy.obj"
	-@erase "$(INTDIR)\WinTeddy.res"
	-@erase "$(OUTDIR)\WinTeddy.exe"
	-@erase "$(OUTDIR)\WinTeddy.ilk"
	-@erase "$(OUTDIR)\WinTeddy.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /Fp"$(INTDIR)\WinTeddy.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC_PROJ=/l 0x807 /fo"$(INTDIR)\WinTeddy.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\WinTeddy.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib /nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)\WinTeddy.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)\WinTeddy.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\WinTeddy.obj" \
	"$(INTDIR)\WinTeddy.res"

"$(OUTDIR)\WinTeddy.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(CFG)" == "WinTeddy - Win32 Release" || "$(CFG)" ==\
 "WinTeddy - Win32 Debug"

!IF  "$(CFG)" == "WinTeddy - Win32 Release"

"WinTeddyInit - Win32 Release" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F .\WinTeddyInit.mak\
 CFG="WinTeddyInit - Win32 Release" 
   cd "."

"WinTeddyInit - Win32 ReleaseCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) CLEAN /F .\WinTeddyInit.mak\
 CFG="WinTeddyInit - Win32 Release" RECURSE=1 
   cd "."

!ELSEIF  "$(CFG)" == "WinTeddy - Win32 Debug"

"WinTeddyInit - Win32 Debug" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) /F .\WinTeddyInit.mak CFG="WinTeddyInit - Win32 Debug"\
 
   cd "."

"WinTeddyInit - Win32 DebugCLEAN" : 
   cd "."
   $(MAKE) /$(MAKEFLAGS) CLEAN /F .\WinTeddyInit.mak\
 CFG="WinTeddyInit - Win32 Debug" RECURSE=1 
   cd "."

!ENDIF 

SOURCE=.\WinTeddy.cpp
DEP_CPP_WINTE=\
	".\resource.h"\
	

"$(INTDIR)\WinTeddy.obj" : $(SOURCE) $(DEP_CPP_WINTE) "$(INTDIR)"


SOURCE=.\WinTeddy.rc
DEP_RSC_WINTED=\
	".\resource.h"\
	".\Teddy.bmp"\
	".\Teddy.rgn"\
	".\Title.bmp"\
	".\WinTeddy.ico"\
	".\XC_heart.cur"\	

"$(INTDIR)\WinTeddy.res" : $(SOURCE) $(DEP_RSC_WINTED) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

