// WinTeddy, version 1.0, 10/97 (for Windows 95/NT)
//
// Written by Claudio Felber, Cybernetic (felber@cybernetic.ch)
//
// WinTeddy is based on xteddy for X Windows.
// Original idea and program by Stefan Gustavson, ISY-LiTH (stegu@itn.liu.se)
//
// The teddy bear images are photos of a Tender Teddy from Gund.
// WinTeddy is funware and may be freely copied and distributed.


#include <windows.h>
#include "resource.h"       // Definitions of resource identificators

HINSTANCE hinst;            // Program instance handle
HWND hwndMain;              // Handle of main teddy window
HMENU hmenuPopup;           // Handle of right mouse button menu
HRGN hrgnTeddy;             // A region defining teddy's shape
HBITMAP hbmTeddy;           // Here's how the teddy looks like
HPALETTE hpalTeddy;         // And here are the colors he uses
LONG nTeddyWidth;           // The dimensions of the teddy bitmap
LONG nTeddyHeight;

// This function calculates a default position for the teddy which
// lets him sit 32 pixels away from the top right border of the
// screen. This is especially handy if the teddy gets lost due
// to a screen resize to a lower resolution.


void DefaultPosition()
{
    // Move the teddy to his default position.

    MoveWindow(hwndMain, GetSystemMetrics(SM_CXSCREEN) - nTeddyWidth - 32, 32,
        nTeddyWidth, nTeddyHeight, TRUE);
}

// Save the current position of the teddy in the registry in order
// to have him reappear where you left him. The registry key used
// is "Software\Cybernetic\WinTeddy\1.0". The position is stored
// in the items "Left" and "Top".

void SavePosition()
{
    // Get the current teddy window position.

    RECT rc;
    GetWindowRect(hwndMain, &rc);

    // Open the registry key and create it if it does not yet exist.

    HKEY hkey;
    RegCreateKey(HKEY_CURRENT_USER, "Software\\Cybernetic\\WinTeddy\\1.0", &hkey);

    // Store the position of the teddy window in the registry.

    RegSetValueEx(hkey, "Left", 0, REG_DWORD, (CONST BYTE*)&rc.left,
        sizeof(rc.left));
    RegSetValueEx(hkey, "Top", 0, REG_DWORD, (CONST BYTE*)&rc.top,
        sizeof(rc.top));

    // Close the registry key since we do no longer use it.

    RegCloseKey(hkey);
}

// Read the position where the teddy has been left from the registry
// and move the teddy window to this location.

void RestorePosition()
{
    // Open the WinTeddy registry key.

    HKEY hkey;
    LONG lResult = RegOpenKey(HKEY_CURRENT_USER, "Software\\Cybernetic\\WinTeddy\\1.0", &hkey);

    // If the key could be opened then read the position.

    if (lResult == ERROR_SUCCESS) {
        DWORD dwSize = sizeof(DWORD);
        DWORD nLeft;
        DWORD nTop;
        RegQueryValueEx(hkey, "Left", NULL, NULL, (LPBYTE)&nLeft, &dwSize);
        RegQueryValueEx(hkey, "Top", NULL, NULL, (LPBYTE)&nTop, &dwSize);
        RegCloseKey(hkey);

        // Move the teddy window to the position we've just read.

        MoveWindow(hwndMain, nLeft, nTop, nTeddyWidth, nTeddyHeight, TRUE);
    }
}

// This function handles messages sent to the small "About" dialog box
// which appears when the teddy bear is clicked with the right mouse
// button. All it does is close the dialog box when a command message
// (obviously originating from the "Wow!" button) is received.

BOOL CALLBACK DialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg) {
        case WM_COMMAND:
            EndDialog(hwndDlg, TRUE);
            break;
    }
    return FALSE;
}

// This function is used to handle messages sent to the teddy bear window.
// It is called by Windows whenever something happens that might be of
// interest to our small friend.

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;         // Used in WM_PAINT to optain a device context
    HBITMAP hbmOld;         // Temporary storage for bitmap and palette
    HPALETTE hpalOld;       // in the device context we're working with
    HDC hdc;                // Device context holding the teddy bitmap
    POINT pt;               // Holds the position of the mouse at right clicks
    UINT nChanges;          // Used for color palette management

    switch(uMsg) {

        // With this message Windows asks us to redraw the window.
        // Note: we also capture WM_NCPAINT here which is used very rarely
        // otherwise but seems to be necessary here to ensure correct repainting
        // of the teddy window when it has been covered by another teddy window.

        case WM_NCPAINT:
        case WM_PAINT:

            // Initiate the paint process and optain a device context
            // which is then accessible through ps.hdc.

            BeginPaint(hWnd, &ps);

            // Activate the color palette used by our teddy bear.

            hpalOld = SelectPalette(ps.hdc, hpalTeddy, TRUE);
            RealizePalette(ps.hdc);

            // Create a duplicate of the device context describing our
            // window, select the teddy bear bitmap into this second
            // device context and from there copy the bitmap to the
            // primary device context.

            hdc = CreateCompatibleDC(ps.hdc);
            hbmOld = (HBITMAP)SelectObject(hdc, hbmTeddy);
            BitBlt(ps.hdc, 0, 0, nTeddyWidth, nTeddyHeight, hdc, 0, 0, SRCCOPY);

            // Cleanup and delete the duplicate device conext.

            SelectObject(hdc, hbmOld);
            DeleteDC(hdc);

            // Restore the former color palette since we might by an inactive
            // window.

            SelectPalette(ps.hdc, hpalOld, TRUE);
            RealizePalette(hdc);

            // Inform Windows that we finished repainting our window.

            EndPaint(hWnd, &ps);
            break;

        // The next two messages are used for palette management. They are sent
        // when Windows wants us to know that somebody has changed the system
        // palette or if it needs to know what palette we are using.

        case WM_PALETTECHANGED:
            if ((HWND)wParam == hWnd)
                break;

        case WM_QUERYNEWPALETTE:

            // Create a device context and activate our palette (just to check
            // if some colors were altered).

            hdc = GetDC(hWnd);
            hpalOld = SelectPalette(hdc, hpalTeddy, FALSE);
            nChanges = RealizePalette(hdc);
            SelectPalette(hdc, hpalOld, TRUE);
            RealizePalette(hdc);
            ReleaseDC(hWnd, hdc);

            // If any colors we use have been changed we might need to redraw
            // our teddy bear in order to have him look cute.

            if (nChanges)
                InvalidateRect(hWnd, NULL, TRUE);
            return nChanges;

        // On selection of a menu item we receive a WM_COMMAND message.

        case WM_COMMAND:
            switch (LOWORD(wParam)) {

                // Display the "About" dialog if the user has selected the 
                // "About" menu item.

                case ID_FILE_ABOUT:
                    DialogBox(hinst, MAKEINTRESOURCE(IDD_ABOUT), hWnd, (DLGPROC)DialogProc);
                    break;

                // Move the teddy to a default position in the top right
                // corner of the screen.

                case ID_FILE_DEFAULTPOS:
                    DefaultPosition();
                    break;

                // Quit the application by destroying the main window (which
                // in turn then terminates the application, see WM_DESTROY).

                case ID_FILE_EXIT:
                    DestroyWindow(hWnd);
                    break;
            }
            break;

        // Since we have extended the system menu of the teddy bear's window
        // to contain two additional commands ("About" and "Default position")
        // we also need to handle menu selections happening there.

        case WM_SYSCOMMAND:
            switch (wParam) {
                case ID_FILE_ABOUT:
                    DialogBox(hinst, MAKEINTRESOURCE(IDD_ABOUT), hWnd, (DLGPROC)DialogProc);
                    break;
                case ID_FILE_DEFAULTPOS:
                    DefaultPosition();
            }
            break;

        // When the left mouse button is being pressed we simulate a pressing
        // of the window title thus switching the teddy window into movement
        // mode.

        case WM_LBUTTONDOWN:
            PostMessage(hWnd, WM_LBUTTONUP, 0, 0);
            PostMessage(hWnd, WM_SYSCOMMAND, SC_MOVE + 1, 0);
            break;

        // Upon a click of the right mouse button we display a small 
        // popup menu.

        case WM_RBUTTONDOWN:

            // Extract the current mouse position.

            pt.x = LOWORD(lParam);
            pt.y = HIWORD(lParam);

            // Convert the window relative "client mouse coordinates to screen
            // coordinates since TrackPopupMenu() wants them to be so.

            ClientToScreen(hWnd, &pt);

            // Now show the popup menu at the current mouse position.

            TrackPopupMenu(GetSubMenu(hmenuPopup, 0), TPM_LEFTALIGN, pt.x, pt.y, 0, hWnd, NULL);
            break;

        // This message is received when the window is being destroyed (e.g. after
        // DestroyWindow() has been called. All we do is save the current position
        // of the teddy's window in the registry and put the message WM_QUIT in
        // the central application message queue to terminate the message loop
        // in WinMain().

        case WM_DESTROY:
            SavePosition();
            PostQuitMessage(0);
            break;
    }

    // The messages we're not interested in are sent to the default Windows
    // message handler where some standard things are done.

    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

// This functions performs all the major program initializations.

BOOL InitApplication(int nCmdShow)
{
    // Load the teddy bitmap from resource data.

    hbmTeddy = (HBITMAP)LoadImage(hinst, MAKEINTRESOURCE(IDB_TEDDY),
        IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_DEFAULTSIZE);

    // Determine the width and height of the bitmap.

    DIBSECTION dbs;
    GetObject(hbmTeddy, sizeof(DIBSECTION), &dbs);

    nTeddyWidth = dbs.dsBm.bmWidth;
    nTeddyHeight = dbs.dsBm.bmHeight;

    // Create a device context and stuff it with the teddy bitmap.
    // This is needed to be able to extract the bitmap colors
    // with GetDIBColorTable() later on.

    HDC hdc = CreateCompatibleDC(NULL);
    HBITMAP hbmOld = (HBITMAP)SelectObject(hdc, hbmTeddy);

    RGBQUAD rgb[256];
    GetDIBColorTable(hdc, 0, 255, rgb);

    // Create a LOGPALETTE structure from the colors taken from
    // the teddy's bitmap. This structure is needed for the
    // color palette creation using the CreatePalette() function.

    struct {
        WORD palVersion;
        WORD palNumEntries;
        PALETTEENTRY palPalEntry[256];
    } lgpl;

    lgpl.palVersion = 0x300;
    lgpl.palNumEntries = 256;

    for (int i = 0; i < 256; i++) {
        lgpl.palPalEntry[i].peRed = rgb[i].rgbRed;
        lgpl.palPalEntry[i].peGreen = rgb[i].rgbGreen;
        lgpl.palPalEntry[i].peBlue = rgb[i].rgbBlue;
        lgpl.palPalEntry[i].peFlags = 0;
    }

    // Create the color palette.

    hpalTeddy = CreatePalette((PLOGPALETTE)&lgpl);

    // Cleanup and free the temporary device context.

    SelectObject(hdc, hbmOld);
    DeleteDC(hdc);

    // Load the mask region data from a RT_USER resource and create
    // a new region shaped like the teddy bitmap.
    
    HRSRC hrsrc = FindResource(NULL, MAKEINTRESOURCE(IDR_TEDDYRGN), "RT_USER");
    HGLOBAL hglb = LoadResource(NULL, hrsrc);
    DWORD dwSize = SizeofResource(NULL, hrsrc);
    HRGN hrgnTeddy = ExtCreateRegion(NULL, dwSize, (PRGNDATA)hglb);

    // Create a window class for the teddy window.

    static char szClassName[] = "WinTeddy";
    static char szWindowTitle[] = "WinTeddy";
    WNDCLASS wc;

    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hinst;
    wc.hIcon = LoadIcon(hinst, MAKEINTRESOURCE(IDI_APP));
    // wc.hCursor = LoadCursor(hinst, MAKEINTRESOURCE(IDC_XC_HEART));
	wc.hCursor = (HCURSOR)LoadImage(hinst, MAKEINTRESOURCE(IDC_XC_HEART), IMAGE_CURSOR, 16, 16, LR_DEFAULTCOLOR);
    wc.hbrBackground = (HBRUSH)NULL;
    wc.lpszMenuName = NULL;
    wc.lpszClassName = szClassName;
    RegisterClass(&wc);

    // And now create the real teddy window based on the class we registered
    // before.

    hwndMain = CreateWindowEx(WS_EX_TOOLWINDOW, szClassName, szWindowTitle,
        WS_POPUP | WS_SYSMENU,
        CW_USEDEFAULT, 0, nTeddyWidth, nTeddyHeight,
        NULL, NULL, hinst, NULL);

    if (!hwndMain)
        return FALSE;

    // Now assign the region defining the teddy's shape to the window.

    SetWindowRgn(hwndMain, hrgnTeddy, FALSE);
	
    // Move the window to a default position. If the registry holds a valid
    // position then it is used instead of the default position i.e. 
    // overwrites it. Afterwards the window is made visible.

    DefaultPosition();
    RestorePosition();
    ShowWindow(hwndMain, nCmdShow);

    // Load the popup menu from a menu resource.

    hmenuPopup = LoadMenu(hinst, MAKEINTRESOURCE(IDR_MENU));

    // Add a separator and two further menu items to the standard
    // system menu associated with our teddy window.

    HMENU hmenuSys = GetSystemMenu(hwndMain, FALSE);
    AppendMenu(hmenuSys, MF_SEPARATOR, 0, NULL);
    AppendMenu(hmenuSys, MF_STRING, ID_FILE_ABOUT, "&About...");
    AppendMenu(hmenuSys, MF_STRING, ID_FILE_DEFAULTPOS, "&Default position");

    return TRUE;
}

// This is the main program entry. Execution starts here.

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;

    // Store the program instance in a global variable and initialize
    // what we need (window, bitmap, palette, region).

    hinst = hInstance;
    if (!InitApplication(nCmdShow))
        return FALSE;

    // Read and dispatch messages to the window procedure until
    // we find the message WM_QUIT.

    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}
